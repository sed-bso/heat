FROM ubuntu:22.04

# Installing as root: docker images are usually set up as root.
# Since some autotools scripts might complain about this being unsafe, we set
# FORCE_UNSAFE_CONFIGURE=1 to avoid configure errors.
ENV FORCE_UNSAFE_CONFIGURE=1
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y \
  sudo \
  bash \
  cpio \
  wget \
  vim \
  emacs \
  tmux \
  curl \
  patch \
  unzip \
  bzip2 \
  mlocate \
  gawk \
  bison \
  flex \
  jq \
  git \
  build-essential \
  gfortran \
  clang \
  clang-tidy \
  autoconf \
  automake \
  cmake \
  cmake-data\
  cmake-curses-gui \
  graphviz \
  doxygen \
  gcovr \
  lcov \
  cppcheck \
  vera++ \
  valgrind \
  python-is-python3 \
  python3-pip \
  python3-numpy \
  python3-matplotlib \
  libopenmpi-dev \
  ffmpeg \
  ffmpeg2theora \
  xsltproc && \
  apt-get autoremove -y

RUN python3 -m pip install --upgrade pip && \
    python3 -m pip install pylint pytest pytest-cov scan-build setuptools

RUN chmod a+rx /root && \
    mkdir -p /root/apps

ENV version_rats 2.4
ENV sha256_rats 2163ad111070542d941c23b98d3da231f13cf065f50f2e4ca40673996570776a
RUN cd /root/apps && \
    wget https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/rough-auditing-tool-for-security/rats-${version_rats}.tgz && \
    echo "${sha256_rats} rats-${version_rats}.tgz" | sha256sum -c - && \
    tar -xzvf rats-${version_rats}.tgz && \
    cd rats-${version_rats} && \
    ./configure && make && sudo make install && \
    rm /root/apps/rats-${version_rats}.tgz

ENV version_drmemory 2.5.0
ENV sha256_drmemory d0f248403b759699435a91500253a65852a29abf761979d351229541beaf20c1
RUN cd /root/apps && \
    wget https://github.com/DynamoRIO/drmemory/releases/download/release_${version_drmemory}/DrMemory-Linux-${version_drmemory}.tar.gz && \
    echo "${sha256_drmemory} DrMemory-Linux-${version_drmemory}.tar.gz" | sha256sum -c - && \
    tar xf DrMemory-Linux-${version_drmemory}.tar.gz && \
    rm /root/apps/DrMemory-Linux-${version_drmemory}.tar.gz

ENV sha256_coverity 0b234a20b25794330e0976d9645e2bfe49f3d28ad1c77da84a3f8aa6afe7c574
RUN cd /root/apps && \
    wget --no-check-certificate https://scan.coverity.com/download/linux64 --post-data "token=XEJaJ1cAnqW-9M_zkmxd7w&project=Heat" -O coverity_tool.tgz && \
    echo "${sha256_coverity} coverity_tool.tgz" | sha256sum -c - && \
    tar xf coverity_tool.tgz && \
    ln -s -f $PWD/cov-analysis-linux64-*/bin/cov-build /usr/local/bin/cov-build && \
    rm /root/apps/coverity_tool.tgz

RUN cd /root/apps && \
    git clone https://github.com/SonarOpenCommunity/sonar-cxx.git && \
    chmod +x /root/apps/sonar-cxx/cxx-sensors/src/tools/vera++Report2checkstyleReport.perl && \
    ln -s /root/apps/sonar-cxx/cxx-sensors/src/tools/vera++Report2checkstyleReport.perl /usr/local/bin/vera++Report2checkstyleReport.perl

ENV version_sonar 4.8.0.2856
ENV sha256_sonar 45a9a54dfe5f58b554e9b40ad3becbf9871a4eddb1c2892b67cf191cdd891754
RUN cd /root/apps && \
    wget https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-${version_sonar}-linux.zip -O sonar.zip && \
    echo "${sha256_sonar} sonar.zip" | sha256sum -c - && \
    unzip sonar.zip && \
    ln -s /root/apps/sonar-scanner-${version_sonar}-linux/bin/sonar-scanner /usr/local/bin/sonar-scanner && \
    rm /root/apps/sonar.zip

RUN groupadd -f -g 1000 gitlab && \
    useradd -u 1000 -g gitlab -d /home/gitlab/ -ms /bin/bash gitlab && \
    mkdir /builds && \
    chown -R gitlab:gitlab /builds && \
    echo "gitlab:gitlab" | chpasswd && adduser gitlab sudo

USER gitlab

# change the default shell to be bash
SHELL ["/bin/bash", "-c"]

# set DRMEMORY path (does not work without using an absolute path)
ENV DRMEMORY /root/apps/DrMemory-Linux-${version_drmemory}/bin64

# default working directory
WORKDIR /builds
