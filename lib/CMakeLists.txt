# in dir lib/
cmake_minimum_required(VERSION 3.1)

add_library(heat heat.c)
set_target_properties(heat PROPERTIES VERSION ${HEAT_VERSION})

# directory where to find headers we depend on
target_include_directories(heat PUBLIC
  $<BUILD_INTERFACE:${HEAT_SOURCE_DIR}/include>
  $<INSTALL_INTERFACE:include>)

# this header is necessary to compile any code using libheat
set_target_properties(heat PROPERTIES PUBLIC_HEADER "${HEAT_SOURCE_DIR}/include/heat.h")

# dependency to libm
find_package(M REQUIRED)
if (TARGET Math::m)
  target_link_libraries(heat PRIVATE Math::m)
endif()

# export the target m
include(export_imported_target)
if (NOT BUILD_SHARED_LIBS)
  export_imported_target(Math m M)
  install(FILES "${CMAKE_CURRENT_BINARY_DIR}/MTargets.cmake"
          DESTINATION lib/cmake/heat)
endif()

# where to copy the libheat at make install + associate an export to this installed target
install(TARGETS heat
        EXPORT heatTargets
        DESTINATION lib
        PUBLIC_HEADER DESTINATION include)

# generate the file containing the specific libheat target + destination at install
install(EXPORT heatTargets
        FILE HeatTargets.cmake
        NAMESPACE heat::
        DESTINATION lib/cmake/heat)

# generate the global config file (including all targets) for the whole heat project
configure_file ("HeatConfig.cmake.in"
                "${CMAKE_CURRENT_BINARY_DIR}/HeatConfig.cmake"
                @ONLY)
# install rule for HeatConfig.cmake file
install(FILES "${CMAKE_CURRENT_BINARY_DIR}/HeatConfig.cmake"
        DESTINATION lib/cmake/heat)